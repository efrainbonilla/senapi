/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

eval("(function ($, _, Drupal, drupalSettings) {\n\tconsole.log($, _, Drupal, drupalSettings);\n\n\tDrupal.behaviors.sticky_header = {\n\t\tattach: function (context, settings) {\n\t\t\tvar selector = drupalSettings.stickynav.selector;\n\t\t\tvar $menu = $(selector);\n\t\t\t$(window).scroll(function () {\n\t\t\t\tvar width = $menu.parent().width();\n\t\t\t\tif ($(window).scrollTop() > Drupal.behaviors.stickynav.breakpoint) {\n\t\t\t\t\t$menu.addClass('navbar-color');\n\t\t\t\t\t$menu.css('width', width);\n\t\t\t\t} else {\n\t\t\t\t\t$menu.removeClass('navbar-color');\n\t\t\t\t\t$menu.css('width', '100%');\n\t\t\t\t}\n\t\t\t});\n\t\t}\n\t};\n})(window.jQuery, window._, window.Drupal, window.drupalSettings);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NjcmlwdHMvbWFpbi5qcz8yZjU4Il0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiAoJCwgXywgRHJ1cGFsLCBkcnVwYWxTZXR0aW5ncykge1xuXHRjb25zb2xlLmxvZygkLCBfLCBEcnVwYWwsIGRydXBhbFNldHRpbmdzKTtcblxuXHREcnVwYWwuYmVoYXZpb3JzLnN0aWNreV9oZWFkZXIgPSB7XG5cdFx0YXR0YWNoOiBmdW5jdGlvbiAoY29udGV4dCwgc2V0dGluZ3MpIHtcblx0XHRcdHZhciBzZWxlY3RvciA9IGRydXBhbFNldHRpbmdzLnN0aWNreW5hdi5zZWxlY3Rvcjtcblx0XHRcdHZhciAkbWVudSA9ICQoc2VsZWN0b3IpO1xuXHRcdFx0JCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHZhciB3aWR0aCA9ICRtZW51LnBhcmVudCgpLndpZHRoKCk7XG5cdFx0XHRcdGlmICgkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPiBEcnVwYWwuYmVoYXZpb3JzLnN0aWNreW5hdi5icmVha3BvaW50KSB7XG5cdFx0XHRcdFx0JG1lbnUuYWRkQ2xhc3MoJ25hdmJhci1jb2xvcicpO1xuXHRcdFx0XHRcdCRtZW51LmNzcygnd2lkdGgnLCB3aWR0aCk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JG1lbnUucmVtb3ZlQ2xhc3MoJ25hdmJhci1jb2xvcicpO1xuXHRcdFx0XHRcdCRtZW51LmNzcygnd2lkdGgnLCAnMTAwJScpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdH07XG59KSh3aW5kb3cualF1ZXJ5LCB3aW5kb3cuXywgd2luZG93LkRydXBhbCwgd2luZG93LmRydXBhbFNldHRpbmdzKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NjcmlwdHMvbWFpbi5qc1xuLy8gbW9kdWxlIGlkID0gMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///0\n");

/***/ })
/******/ ]);