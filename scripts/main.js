(function($, _, Drupal, drupalSettings) {
	console.log($, _, Drupal, drupalSettings);

	Drupal.behaviors.sticky_header = {
		attach: function(context, settings) {
      		var selector = drupalSettings.stickynav.selector;
      		var $menu = $(selector);
			$(window).scroll(function() {
				var width = $menu.parent().width();
				if ($(window).scrollTop() > Drupal.behaviors.stickynav.breakpoint) {
					$menu.addClass('navbar-color');
					$menu.css('width', width);
				} else {
					$menu.removeClass('navbar-color');
					$menu.css('width', '100%');
				}
			});
		}
	};

})(window.jQuery, window._, window.Drupal, window.drupalSettings);