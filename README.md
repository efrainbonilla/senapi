# Drupal 8 theme SENAPI

## Requerimientos
- Nodejs
- Gulp-cli: npm install -g gulp-cli

## Instalación
- npm install


## Dupal Module
- block_class 
- pathauto
- libraries

##### Manejo de imagenes
- slick (Slider, carrosel...)
- slick_extras
- slick_views
- blazy (efecto imagen cargando)
- blazy_ui
- juicebox

### Desarrollo
- admin_toolbar
- devel
